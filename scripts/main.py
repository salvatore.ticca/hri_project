import os, sys
import naoqi
import threading
import time
import movie_review
import tickets

selected_film = ""
selected_num = 0
selected_hour = 0

pdir = os.getenv('MODIM_HOME')
sys.path.append(pdir + '/src/GUI')

pdir = os.getenv('PEPPER_TOOLS_HOME')
sys.path.append(pdir+ '/cmd_server')

import pepper_cmd
from pepper_cmd import *

from ws_client import *
import ws_client

PATH=__file__

def greeting():
    im.init()
    im.display.loadUrl('greetings.html')
    im.executeModality('TEXT_title','Hello i am Pepper')

    im.executeModality('TTS','Hello')
    im.executeModality('TTS','I am Pepper')
    
    im.executeModality('GESTURE','animations/Stand/Gestures/Hey_6')
    #im.executeModality('GESTURE','animations/Stand/Gestures/Me_1')

def finish():
    im.init()
    im.display.loadUrl('index.html')

"""
    Show the user the menu and ask to chose between the two services, Info & Tickets or Review Movie
"""
def select_services():
    im.init()
    im.display.loadUrl('menu.html')
    im.executeModality('TEXT_subtitle',' ')
    service = im.ask('select_service', timeout=30)

    if service=='timeout':
        im.robot.memory_service.insertData('SERVICE','END')    
    else:
        if(service=='back'):
            im.executeModality('TEXT_default','Goodbye')
            im.executeModality('TTS','Goodbye')
            time.sleep(2)
            im.robot.memory_service.insertData('SERVICE','END')   
        else:
            im.robot.memory_service.insertData('SERVICE',service)

"""
    Ask the user to confirm he needs something from the robot.
    This way it is possible to stop the interaction early if an uninterested user has been engaged
"""
def offer_service():
    im.init()
    im.display.loadUrl('menu.html')
    if im.robot.memory_service.getData('SERVICE') == 'START':
        im.executeModality('TEXT_title','Do you need help?')
        im.executeModality('TTS','Do you need help?')
        im.executeModality('TTS','I can show you the program and help buy a ticket, or we can talk about a movie you watched')
    else:
        im.executeModality('TEXT_title','Do you need something else?')
        im.executeModality('TTS','Do you need something else?')

    a = im.ask('offer_service', timeout=15)
    
    if a=='timeout':
        im.robot.memory_service.insertData('SERVICE','END')   
    else:
        if(a=='yes'):
            im.robot.memory_service.insertData('SERVICE','select_service')
        else:
            im.executeModality('TEXT_default','Ok, Goodbye')
            im.executeModality('TTS','Ok, Goodbye')
            time.sleep(2)
            im.display.loadUrl('index.html')

"""
    Show the movie list and ask the user to choose the one he is interested in
"""
def select_movie():
    im.init()
    im.display.loadUrl('select_film.html')
    film = im.ask('select_film', timeout=15)
    
    if film=='timeout':
        im.robot.memory_service.insertData('SERVICE','END')    
    else:
        if(film=='back'):
            im.robot.memory_service.insertData('SERVICE','select_service') 
        else:
            im.executeModality('TTS','Ok, here we go')
            im.robot.memory_service.insertData('MOVIE',film)  
            im.robot.memory_service.insertData('SERVICE','movie_info') 

"""
    Display the information about the specific movie chosen by the user
"""   
def movie_info():
    im.init()
    film=im.robot.memory_service.getData('MOVIE')
    im.display.loadUrl('{}.html'.format(film))
    #im.executeModality('TTS','Here we are')

    tickets = im.ask('buy_ticket', timeout=60)
    if tickets == "timeout":
        im.robot.memory_service.insertData('SERVICE','END')   
    else:
        if (tickets=="yes"):
            im.robot.memory_service.insertData('SERVICE','screen_time')    
        else:
            im.robot.memory_service.insertData('SERVICE','select_movie')   

"""
    Ask the  user to select the number of tickets.
    If there areen't enough seats to fulfill his request a message is displayed to inform the user
"""
def select_tickets():
    im.init()
    
    num = im.ask('num_tickets_buy', timeout=30)
    im.robot.memory_service.insertData('SERVICE','END')  
    max_tickets=robot.memory_service.getData('FREE_SEATS')

    if num == "timeout":
        im.robot.memory_service.insertData('SERVICE','END')  
    else:
        if (num == "cancel"):
            im.robot.memory_service.insertData('SERVICE','movie_info')  
        else:
            num=int(num)
            if(num<=max_tickets):
                im.robot.memory_service.insertData('SERVICE','confirm_order')  
                im.robot.memory_service.insertData('NUM_TICKETS',num)  
            else:
                im.ask("no_tickets_left",timeout=3)
                im.robot.memory_service.insertData('SERVICE','select_tickets')  

        #except:
        #im.executeModality('TTS','I dont understand')

"""
    Ask the user to select the time at which he wants to watch the movie
"""
def select_screentime():
    im.init()
    film=im.robot.memory_service.getData('MOVIE')
    im.display.loadUrl('screen_time_{}.html'.format(film))

    time = im.ask('select_screen_time', timeout=30)
    if(time=='timeout'):
        im.robot.memory_service.insertData('SERVICE','END')   
    elif(time == '18'):
        im.robot.memory_service.insertData('SERVICE','select_tickets')
        im.robot.memory_service.insertData('HOUR','18:00')
    elif(time == '21'):
        im.robot.memory_service.insertData('SERVICE','select_tickets')
        im.robot.memory_service.insertData('HOUR','21:00')
    else:
        im.robot.memory_service.insertData('SERVICE','movie_info')
        #select_tickets(film,18)

"""
    Ask the user to confirm his order
"""
def confirm_order():
    im.init()
    im.display.loadUrl('confirm_order.html')
    film=im.robot.memory_service.getData('MOVIE')
    hour=im.robot.memory_service.getData('HOUR')
    num=im.robot.memory_service.getData('NUM_TICKETS')  
    order="{} tickets for {} at {} ".format(num,film.capitalize(),hour)
    price="Total price is {}$".format(num*6.5)
    im.executeModality('TEXT_subtitle',order)
    im.executeModality('TEXT_price',price)
    im.executeModality('TTS',order)

    code = im.ask('confirm_order', timeout=30)

    if code == "timeout":
        im.robot.memory_service.insertData('SERVICE','END')  
    elif(code=='yes'):
        im.executeModality('TTS','Ok')
        im.robot.memory_service.insertData('SERVICE','buy_tickets')  
    else:
        im.robot.memory_service.insertData('SERVICE','movie_info')  
        im.executeModality('TTS','Ok, then lets go back to the movie page')

"""
    Show to the user the code neccessary to pay the tickets at the cash desk.
"""
def buy_tickets():
    im.init()
    im.display.loadUrl('code_to_pay.html')

    code = im.ask('take_code', timeout=30)

    im.executeModality('TTS','Goodbye')
    time.sleep(2)
    im.robot.memory_service.insertData('SERVICE','END')  

"""
    Manage the flow of the interaction
"""
def engagePerson(robot):
    mws = ModimWSClient()
    mws.setDemoPathAuto(PATH)
    mws.run_interaction(greeting)
    
    robot.memory_service.insertData('SERVICE','START') 
    mws.run_interaction(offer_service)
    service=''

    #At each step the memory of the robot is checked and the next step of the interaction is selected
    #If the user decide to stop the interaction or leave and an action goes in timeout, the ineraction is terminated
    while service!='END':
        try:
            service=robot.memory_service.getData('SERVICE')
        except:
            service='END'

        robot.memory_service.insertData('SERVICE','END') 

        if(service=='select_movie'):
            mws.run_interaction(select_movie)
        elif(service=='select_service'):
            mws.run_interaction(select_services)
        elif(service=='review'):
            mws.run_interaction(movie_review.MovieName)
            mws.run_interaction(movie_review.ReviewMovie)
            robot.memory_service.insertData('SERVICE','offer_service') 
        elif(service=='movie_info'):
            mws.run_interaction(movie_info)
        elif(service=='screen_time'):
            mws.run_interaction(select_screentime)
        elif(service=='select_tickets'):
            #load from the memory the movie and time selected by the user and check the number of available seats for that spectacle
            film=robot.memory_service.getData('MOVIE')
            hour=robot.memory_service.getData('HOUR')
            robot.memory_service.insertData('FREE_SEATS',tickets.check_seats(film,hour))  
            print('free_seats: ',tickets.check_seats(film,hour))
            mws.run_interaction(select_tickets)
            robot.memory_service.insertData('FREE_SEATS',None)  
        elif(service=='confirm_order'):
            mws.run_interaction(confirm_order)
        elif(service=='buy_tickets'):
            mws.run_interaction(buy_tickets)
        elif(service=='offer_service'):
            mws.run_interaction(offer_service)

    mws.run_interaction(finish)
    #cancel from the memory the data used to manage the interaction and the choices of the last user
    reset_memory_keys(robot)
    print('interaction end')

def sonarHandler(callback,robot):
    print('starting sensor monitoring')
    robot.startSensorMonitor()
    prevSonar = 0.0
    confidence=0
    delta=1.0 #1 m/s of threshold
    while True:
        p = robot.sensorvalue('frontsonar')
        speed= (abs(prevSonar-p)/0.5) if (prevSonar > 0 and p > 0) else 0
        print('speed: {:.2f}'.format(speed),'confidence: ',confidence)
        #print('p: ',p,'prev: ',prevSonar)

        if(prevSonar>0 and p <= prevSonar and speed<delta):
            confidence+=1
            if(confidence > 10 and p <= 1):
                #if the person is in front of the robot from a while (around 5s)
                #or has approached the robot at slow speed, he likely wants to interact
                callback(robot)
                confidence=0
                prevSonar=p=0
        else:
            #reset the confidence if the user goeas back 
            #or if a fast movement is detected (maybe someone else passed in front of the robot)
            confidence=0
        
        time.sleep(0.5)#check once every 500ms
        prevSonar=p

#util function to avoid using the simulator when testing the interactions
def startInteraction(callback,robot):
    callback(robot)

def reset_memory_keys(robot):
    robot.memory_service.insertData('SERVICE',None)
    robot.memory_service.insertData('MOVIE',None)
    robot.memory_service.insertData('HOUR',None)
    robot.memory_service.insertData('NUM_TICKETS',None)   

def main():
    begin() # connect to robot/simulator with IP in PEPPER_IP env variable

    #start the thread that monitors the environment through the front sonar
    sonar_thread=threading.Thread(target=sonarHandler,args=(engagePerson,pepper_cmd.robot))
    #sonar_thread=threading.Thread(target=startInteraction,args=(engagePerson,pepper_cmd.robot))
    
    sonar_thread.start()
    print('sonar service started')
    sonar_thread.join()

    print('app stopping')
    end()

if __name__ == "__main__":
    print(PATH)
    main()