# Sonar simulation using memory keys
#
# Device/SubDeviceList/Platform/Front/Sonar/Sensor/Value
# Device/SubDeviceList/Platform/Back/Sonar/Sensor/Value

import qi
import argparse
import sys
import time
import threading
import os
import random
import math

memkey = {
    'SonarFront': 'Device/SubDeviceList/Platform/Front/Sonar/Sensor/Value',
    'SonarBack':  'Device/SubDeviceList/Platform/Back/Sonar/Sensor/Value' }


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--pip", type=str, default=os.environ['PEPPER_IP'],
                        help="Robot IP address.  On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--pport", type=int, default=9559,
                        help="Naoqi port number")
    parser.add_argument("--sensor", type=str, default="SonarFront",
                        help="Sensor: SonarFront, SonarBack")
    parser.add_argument("--start_value", type=float, default=3.5,
                        help="initial distance (m)")
    parser.add_argument("--end_value", type=float, default=0.75,
                        help="final distance (m)")
    parser.add_argument("--duration", type=float, default=3.0,
                        help="Duration of the event"),
    parser.add_argument("--crowd", type=bool, default=False,
                        help="if true other people may pass in front of the robot and cause random sonar values")                 

    args = parser.parse_args()
    pip = args.pip
    pport = args.pport

    #Starting application
    try:
        connection_url = "tcp://" + pip + ":" + str(pport)
        app = qi.Application(["SonarSim", "--qi-url=" + connection_url ])
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + pip + "\" on port " + str(pport) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

    app.start()
    session = app.session

    #Starting services
    memory_service  = session.service("ALMemory")

    val = None
    try:
        val = float(args.start_value)
        end_val=float(args.end_value)
        duration= int(args.duration* 10) #10 updates per second
        incr=(end_val - val)/ duration
        print('Incr: ',incr)

    except:
        print("ERROR: value not numerical")
        return

    try:
        mkey = memkey[args.sensor]
        print("Sonar %s = %f" %(args.sensor,val))
        print('End value: ',end_val, 'duration:',duration)
        
        for i in range(0,duration+1):
            random_measure=random.uniform(1,5)#5m max range of the sonar 
            if(args.crowd == True and random_measure < val):
                memory_service.insertData(mkey,random_measure)
                print('other people*: {:.4f}'.format(random_measure))
            else:
                memory_service.insertData(mkey,val)
                print('current val: {:.4f}'.format(val))
            time.sleep(0.1)
            val+=incr
        
        time.sleep(10)
        memory_service.insertData(mkey,0.0)  
        print('reset sonar') 
    except:
        print("ERROR: Sensor %s unknown" %args.sensor)

if __name__ == "__main__":
    main()