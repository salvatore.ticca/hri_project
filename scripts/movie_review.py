import os, sys
import pickle
import json

try:
    import requests 
except:
    import subprocess
    subprocess.check_call([sys.executable, "-m", "pip", "install", "requests"])
    import requests

pdir = os.getenv('MODIM_HOME')
sys.path.append(pdir + '/src/GUI')
ModimDir=pdir+'/src/GUI'

####The path of Our playground  and Modim server/client is different , so if we need to access pickle of Modim from playground, we go to that dir

from ws_client import *
import ws_client

# Definition of interaction functions



def HeadTouch():
    import pickle

    im.init()
    im.display.loadUrl('layout.html')

    time.sleep(2)

    # Setting HTML element of the web page
    im.executeModality('TEXT_title','User Movie FeedBack')
    im.executeModality('TEXT_default','Touch my head if you want to Give a Review')
    im.executeModality('IMAGE','img/Pepper.jpeg')

    # Using TTS service of the robot to speak
    #im.executeModality('TTS','Hello There, i would like your feedback on the Movie, May i know your Name')
    im.robot.say("Touch my head to start the game")

    im.robot.startSensorMonitor()

    headTouched = False
    while not headTouched:
      p = im.robot.sensorvalue()
      headTouched = p[3]>0   # head sensor

    with open('headTouched.pickle', 'wb') as f:
        pickle.dump(headTouched, f)

def MovieName():
    import pickle

    im.init()
    im.display.loadUrl('review.html')

    MovieNames=["sonic","dolittle","mulan"]
    im.executeModality('TEXT_title','Movie Feedback')
    im.executeModality('TEXT_default','What movie did you watch?')
    im.executeModality('TTS','What movie did you watch?')

    #im.executeModality('IMAGE','img/Pepper.jpeg')



    # Since we are using Fake ASR, as we dont have the speech service available. 
    # so pepper_cmd.py stores whatever we write inside write script , 
    # and there is no implementation of vocabulary



    im.executeModality('ASR',MovieNames)
    im.executeModality('BUTTONS',[ [u,u.capitalize()] for u in MovieNames])

    # wait for answer

    
    Result=False
    while(not Result):
        a = im.ask(actionname=None, timeout=60)
        print(a)
        if a=='timeout':
            Result=False
        else:
            Result=True

        with open('MovieNames.pickle', 'wb') as f:
            print(a)
            pickle.dump(a, f)



def ReviewMovie():
    import pickle
    import requests 
    import json
   
    with open('MovieNames.pickle', 'rb') as f:
        MovieName = pickle.load(f)

    im.executeModality('TEXT_title','Review '+MovieName)
    im.executeModality('IMAGE','img/'+MovieName.lower()+'_poster.jpg')
    im.executeModality('TEXT_default','Please tell me about '+MovieName)
    

    vocabulary=[]
    answer = im.robot.asr(vocabulary,60)
    print(answer)

    data = {'Review':answer,'Language':"english"} 
    r = requests.post(url = "http://0.0.0.0:5001/Mood", params = data) 
    json_data = json.loads(r.text)
    moodDetection= json_data["Mood"]

    if(moodDetection == "Bored"):
        #im.executeModality('TEXT_title',UserName+" is "+moodDetection)
        im.executeModality('TEXT_default','I am sorry you feel bored, I will forward your review to the Department')
        im.executeModality('TTS','I am sorry you feel bored, I will forward your review to the Department')
        im.executeModality('IMAGE','img/'+"Bored.png")

    if(moodDetection == "Sad"):
        #im.executeModality('TEXT_title',UserName+" is "+moodDetection)
        im.executeModality('TEXT_default','I am sorry you feel sad, I will forward your review to the Department .<br> Would like to see a Dance ?')
        im.executeModality('TTS','I am sorry you feel sad, I will forward your review to the Department .Would like to see a Dance ?')
        im.executeModality('IMAGE','img/'+"sad.jpg")
        im.executeModality('BUTTONS',[['yes','Yes'],['no','No']])
        im.executeModality('ASR',['yes','no'])

        a = im.ask(actionname=None, timeout=15)

        if a=='yes':
            im.executeModality('TEXT_default','!!! Dancing !!!')
            im.robot.dance()
        else:
            im.executeModality('TEXT_default','OK. No dancing')

        time.sleep(2)
        im.executeModality('TEXT_default','Bye bye')


    if(moodDetection == "Happy"):
        #im.executeModality('TEXT_title',UserName+" is "+moodDetection)
        im.executeModality('TEXT_default','I am glad you are happy and enjoyed the movie, I will forward your review to the Department')
        im.executeModality('TTS','I am glad you are happy and enjoyed the movie, I will forward your review to the Department')
       
        im.executeModality('IMAGE','img/'+"Happy.jpeg")

    if(moodDetection == "Excited"):
        #im.executeModality('TEXT_title',UserName+" is "+moodDetection)
        im.executeModality('TEXT_default','I See you are very excited, keep up the spirit. ')
        im.executeModality('TTS','I See you are very excited, keep up the spirit.')
       
        im.executeModality('IMAGE','img/'+"Excited.png")

    if(moodDetection == "Angry"):
        #im.executeModality('TEXT_title',UserName+" is "+moodDetection)
        im.executeModality('TEXT_default','You seem really angry, Please do write a complaint to IMDB reviews')
        im.executeModality('TTS','You seem really angry, Please do write a complaint to IMDB reviews')
       
        im.executeModality('IMAGE','img/'+"Angry.jpeg")






def e34():
    im.executeModality('TEXT_default','Do you want me to dance?')

    im.executeModality('ASR',['yes','no'])

    # wait for answer
    a = im.ask(actionname=None, timeout=15)

    if a=='yes':
        im.executeModality('TEXT_default','!!! Dancing !!!')
        im.robot.dance()
    else:
        im.executeModality('TEXT_default','OK. No dancing')

    time.sleep(2)
    im.executeModality('TEXT_default','Bye bye')


# main

if __name__ == "__main__":

    # connect to local MODIM server
    mws = ModimWSClient()
    mws.setDemoPathAuto(__file__)

    
    




    




    # run interaction functions

    result=mws.run_interaction(HeadTouch) # blocking

    with open(ModimDir+'/headTouched.pickle', 'rb') as f:
        headTouched = pickle.load(f)

    if(headTouched):
        mws.run_interaction(Username) 


    mws.run_interaction(ReviewMovie)

    #mws.run_interaction(e34) 



