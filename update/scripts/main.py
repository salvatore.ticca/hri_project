import os, sys
import naoqi
import threading
import time

import tickets

selected_film = ""
selected_num = 0
selected_hour = 0

pdir = os.getenv('MODIM_HOME')
sys.path.append(pdir + '/src/GUI')

pdir = os.getenv('PEPPER_TOOLS_HOME')
sys.path.append(pdir+ '/cmd_server')

import pepper_cmd
from pepper_cmd import *

from ws_client import *
import ws_client

PATH=__file__

def greeting():
    im.init()
    im.display.loadUrl('greetings.html')
    im.executeModality('TEXT_title','Hello i am Pepper')

    im.executeModality('TTS','Hello')
    im.executeModality('GESTURE','animations/Stand/Gestures/Hey_6')

    im.executeModality('TTS','I am Pepper')
    im.executeModality('GESTURE','animations/Stand/Gestures/Me_1')
    
def offer_service():
    im.init()
    im.display.loadUrl('menu.html')
    a = im.ask('offer_service', timeout=15)
    
    if a=='timeout':
        im.display.loadUrl('index.html')   
    else:
        if(a=='yes'):
            select_services()
        else:
            im.executeModality('TEXT_default','Ok, Goodbye')
            im.executeModality('TTS','Ok, Goodbye')
            time.sleep(2)
            im.display.loadUrl('index.html')

def select_services():
        im.executeModality('TEXT_subtitle',' ')
        service = im.ask('select_service', timeout=15)

        if service=='timeout':
            im.display.loadUrl('index.html')   
        else:
            im.robot.memory_service.insertData('SERVICE',service)

def select_film_info():
    im.init()
    im.display.loadUrl('select_film.html')
    film = im.ask('select_film', timeout=15)
    
    if film=='timeout':
        im.display.loadUrl('index.html')   
    else:
        if(film=='dolittle_film'):
            dolittle_info()
        elif (film == "sonic_film"):
            sonic_info()
        elif (film =="mulan_film"):
            mulan_info()

def dolittle_info():
    im.init()
    im.display.loadUrl('dolittle.html')
    tickets = im.ask('buy_ticket_dolittle', timeout=15)

    if tickets == "timeout":
        im.display.loadUrl('index.html')
    else:
        if (tickets=="yes"):
        	select_screentime("dolittle")
        else:
        	select_film_info()   

def sonic_info():
    im.init()
    im.display.loadUrl('sonic.html')
    tickets = im.ask('buy_ticket_sonic', timeout=15)

    if tickets == "timeout":
        im.display.loadUrl('index.html')
    else:
        if (tickets=="yes"):
        	select_screentime("sonic")
        else:
        	select_film_info()   

def mulan_info():
    im.init()
    im.display.loadUrl('mulan.html')
    tickets = im.ask('buy_ticket_mulan', timeout=15)

    if tickets == "timeout":
        im.display.loadUrl('index.html')
    else:
        if (tickets=="yes"):
        	select_screentime("mulan")
        else:
        	select_film_info()  

def select_tickets(film,hour):
    im.init()
    num = im.ask('num_tickets_buy', timeout=15)

    if num == "timeout":
        im.display.loadUrl('index.html')
    else:
        if (num=="one_ticket"):
            res = tickets.check_seats(film,1,hour)
            if (res == "done"):
                buy_tickets(1,film,hour)
            else:
                im.ask("no_tickets_left",timeout=15)
                select_tickets(film,hour)
        elif (num=="two_tickets"):
            res = tickets.check_seats(film,2,hour)
            if (res == "done"):
        	   buy_tickets(2,film,hour)
            else:
                im.ask("no_tickets_left",timeout=15)
                select_tickets(film,hour)
        elif (num=="three_tickets"):
            res = tickets.check_seats(film,3,hour)
            if (res == "done"):
        	   buy_tickets(3,film,hour)
            else:
                im.ask("no_tickets_left",timeout=15)
                select_tickets(film,hour)
        elif (num=="four_tickets"):
            res = tickets.check_seats(film,4,hour)
            if (res == "done"):
        	   buy_tickets(4,film,hour)
            else:
                im.ask("no_tickets_left",timeout=15)
                select_tickets(film,hour)
        elif (num=="five_tickets"):
            res = tickets.check_seats(film,5,hour)
            if (res == "done"):
        	   buy_tickets(5,film,hour)
            else:
                im.ask("no_tickets_left",timeout=15)
                select_tickets(film,hour)
        elif (num=="six_tickets"):  
            res = tickets.check_seats(film,6,hour)
            if (res == "done"):
        	   buy_tickets(6,film,hour)
            else:
                im.ask("no_tickets_left",timeout=15)
                select_tickets(film,hour)

def select_screentime(film):
    im.init()
    if (film=="dolittle"):
    	im.display.loadUrl('screen_time_dolittle.html')
    elif (film=="sonic"):
    	im.display.loadUrl('screen_time_sonic.html')
    elif(film=="mulan"):
     	im.display.loadUrl('screen_time_mulan.html')
    
    time = im.ask('select_screen_time', timeout=15)

    if time=='timeout':
        im.display.loadUrl('index.html')  
    elif (time == "18"):
    	select_tickets(film,18)
    elif (time=="21"):
    	select_tickets(film,21)

def buy_tickets(num,film,hour):
    im.init()
    im.display.loadUrl('code_to_pay.html')
    code = im.ask('select_film', timeout=15)

    if code=='timeout':
        im.display.loadUrl('index.html')   

def select_film_buy():
	im.init()
	im.display.loadUrl('select_film_buy.html')
	film = im.ask('select_film', timeout=15)
    
	if film=='timeout':
		im.display.loadUrl('index.html')   
	else:

		time = im.ask('select_screen_time', timeout=15)

		if time=="timeout":
			im.display.loadUrl('index.html')
		else:
			select_tickets(film,time)


def engagePerson(robot):
    mws = ModimWSClient()
    mws.setDemoPathAuto(PATH)
    mws.run_interaction(greeting)
    mws.run_interaction(offer_service)

    try:
        service=robot.memory_service.getData('SERVICE')
    except:
        service='None'
    
    if(service=='movie_info'):
        print('show movie info')
        select_film_info()
    elif(service=='ticket'):
        print('help buy tickets')
    elif(service=='review'):
        print('review a movie')

    robot.memory_service.insertData('SERVICE',None)
    print('interaction end')

def sonarHandler(callback,robot):
    print('starting sensor monitoring')
    robot.startSensorMonitor()
    prevSonar = 0.0
    confidence=0
    delta=1.0 #1 m/s of threshold
    while True:
        p = robot.sensorvalue('frontsonar')
        speed= (abs(prevSonar-p)/0.5) if (prevSonar > 0 and p > 0) else 0
        print('speed: ',speed,'confidence: ',confidence,'p: ',p,'prev: ',prevSonar)
        if(prevSonar>0 and p <= prevSonar and speed<delta):
            confidence+=1
            if(confidence > 10 and p <= 1):
                #the person is in front of the robot from a while (around 5s)
                #or has approached the robot at slow speed, he likely want to interact
                callback(robot)
                confidence=0
                prevSonar=p=0
        else:
            confidence=0#reset the confidence if the user goeas back 
                        #or if a too fast movement is detected
                        #(maybe someone else passed in front of the robot)
        time.sleep(0.5)#check once every 500ms
        prevSonar=p

#util function to avoid using the simulator when testing the interactions
def startInteraction(callback,robot):
    callback(robot)

def main():
    begin() # connect to robot/simulator with IP in PEPPER_IP env variable
    #sonar_thread=threading.Thread(target=sonarHandler,args=(engagePerson,pepper_cmd.robot))
    sonar_thread=threading.Thread(target=startInteraction,args=(engagePerson,pepper_cmd.robot))
    
    sonar_thread.start()
    print('sonar service started')
    for i in range(40):
        #print(i)
        time.sleep(1)

    print('app stopping')
    #sonar_thread.join()
    end()

if __name__ == "__main__":
    print(PATH)
    main()