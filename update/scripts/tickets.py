import json
import os 

path=os.path.dirname(__file__)
print(path)

def check_seats(film:str, num:int,hour:int):
	with open(os.path.join(path,'seats.json'),'r') as f:
		data = json.load(f)
		if (film=="dolittle"):
			h = 0
		elif (film=="sonic"):
			h = 2
		elif(film=="mulan"):
			h = 4
		if (hour == 21):
			h += 1
		free = data[h]["free_seats"]
		if (free >= num):
			data[h]["free_seats"] = free- num
			return "done"
		else:
			return "full"
