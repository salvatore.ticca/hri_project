function loadJSON(path,callback,film){
    var xobj = new XMLHttpRequest();
          xobj.overrideMimeType("application/json");
      xobj.open('GET', path, true); // Replace 'my_data' with the path to your file
      xobj.onreadystatechange = function () {
            if (xobj.readyState == 4 && xobj.status == "200") {
              // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
              callback(film,xobj.responseText);
            }
      };
      xobj.send(null);  
  }
  
function JSONloaded(film,json){
    var seats = JSON.parse(json);
    schedule=document.getElementById("schedule")
    j=0
    for (i = 0; i < seats.length; i++){
        if(seats[i]['film']==film){
        j+=1
        line=document.createElement("p")
        line.classList.add("screen");
        content="Room "+seats[i]['room']+" at "+seats[i]['time'] + " - " + seats[i]['free_seats']+" seats left"
        line.appendChild(document.createTextNode(content))
        schedule.appendChild(line);
        }
    }
}

